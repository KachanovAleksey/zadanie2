clc;
clear;
close all;

%%
f=140e6;
N=8;
t=100e-9;
pause=100e-9;
fd=2e9;

%%
n=round(t*fd);
n_pause=round(pause*fd);
s=[ones(1,n) zeros(1,n_pause)];
Signal=repmat(s,1,N);
td=(0:length(Signal)-1)./fd;
SVCH_Signal=cos(2*pi*f.*td );
CarrierSignal=Signal.*SVCH_Signal;

figure;
plot(td,CarrierSignal,td,Signal,'r--');
grid on;
title('Сигнал');
xlabel('Время');
ylabel('Амплитуда');

%%
r=2^nextpow2(n+n_pause);
%r=2^10;
S=fft(CarrierSignal,r);
SS=fd*(0:(r/2))/r;
S1=abs(S);

figure;
plot(SS,S1(1:r/2+1));
%xlim([0 2*f]);
grid on;
title('Амплитудный спектр');
xlabel('Частота');
ylabel('Амплитуда');

figure;
S2=angle(S);
plot(SS,S2(1:r/2+1));
%xlim([0 3*f]);
grid on;
title('Фазовый спектр');
xlabel('Частота');
ylabel('Фаза');



%%
figure;
AKF_t=[flip(td(2:length(CarrierSignal)))*(-1) td(1:length(CarrierSignal))];
plot(AKF_t,abs(xcorr(CarrierSignal)/max(xcorr(CarrierSignal))));
grid on;
title('АКФ');
xlabel('Сдвиг');
ylabel('АКФ');

%%
figure;
prf = 1e6;
[afmag,delay,doppler] = ambgfun(CarrierSignal,fd,prf);
mesh (delay,doppler,afmag);
ylim ([-1e5 1e5]);
grid on;
title('Ф-ия неопределенности для смещения по доплеру');
xlabel('Задержка');
ylabel('Частота Допплера');
zlabel('Нормированная неоднозначность');
