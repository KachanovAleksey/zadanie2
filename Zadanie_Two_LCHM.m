clc;
clear;
close all;
%%
fmin=130e6;
fmax=170e6;
fd=120e6;
%fd=2e9;
tstart=0;
tstop=10e-6;

%%

t=tstart:1/fd:tstop;
Signal=chirp(t,fmin,tstop,fmax);

figure;
plot(t,Signal);
grid on;
title('Сигнал');
xlabel('Время');
ylabel('Амплитуда');

%%

figure;
n=round(tstop*fd);
%r=2^nextpow2(n);
r=2^10;
S=fft(Signal,r);
SS=fd*(0:(r/2))/r;
S1=abs(S/r);
plot(SS,S1(1:r/2+1));
%xlim([0 3e8]);
grid on;
title('Амплитудный спектр');
xlabel('Частота');
ylabel('Амплитуда');

figure;
S2=angle(S);
plot(SS,S2(1:r/2+1));
%xlim([0 3e8]);
grid on;
title('Фазовый спектр');
xlabel('Частота');
ylabel('Фаза');

%%

AKF_t=[flip(t(2:length(t)))*(-1) t(1:length(t))];
figure;
plot(AKF_t,abs(xcorr(Signal)/max(xcorr(Signal))));
grid on;
title('АКФ');
xlabel('Сдвиг');
ylabel('АКФ');

%%

figure;
prf = 1e6;
[afmag,delay,doppler] = ambgfun(Signal,fd,prf);
mesh (delay,doppler,afmag);
ylim([-1e5 1e5]);
grid on;
title('Ф-ия неопределенности для смещения по доплеру');
xlabel('Задержка');
ylabel('Частота Допплера');
zlabel('Нормированная неоднозначность');
