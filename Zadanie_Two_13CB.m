clc;
clear;
close all;
%%
A=[1 1 1 1 1 -1 -1 1 1 -1 1 -1 1];
f=158e6;
T=1/f;
fd=2.5e9;

%%
N=round(T*fd);
td=(0:1e3-1)./fd;
buffer=[];
 for i=1:length(A)
     buffer=[buffer repmat(A(i),1,N)];
 end
  s=buffer;
Signal=sin(2*pi*f.*td);
CarrierSignal=s.*Signal(1:length(s));
%%

figure;
plot(td(1:length(CarrierSignal)),CarrierSignal,td(1:length(CarrierSignal)),s,'r--');
grid on;
title('Сигнал');
xlabel('Время');
ylabel('Амплитуда');


%%

figure;
%r=2^nextpow2(N);
r=2^10;
S=fft(CarrierSignal,r);
SS=fd*(0:(r/2))/r;
S1=abs(S);
plot(SS,S1(1:r/2+1));
xlim([0 3.2e8]);
grid on;
title('Амплитудный спектр');
xlabel('Частота');
ylabel('Амплитуда');

figure;
S2=angle(S);
plot(SS,S2(1:r/2+1));
xlim([0 3.2e8]);
grid on;
title('Фазовый спектр');
xlabel('Частота');
ylabel('Фаза');

%%
AKF_t=[flip(td(2:length(CarrierSignal)))*(-1) td(1:length(CarrierSignal))];

figure ;
plot(AKF_t,abs(xcorr(CarrierSignal)/max(xcorr(CarrierSignal))));
grid on;
title('АКФ');
xlabel('Сдвиг');
ylabel('АКФ');

%%

figure;
prf = 1e9;
[afmag,dellay,doppler]=ambgfun(CarrierSignal,fd,prf);
mesh (dellay,doppler,afmag);
ylim ([-1e5 1e5]);
grid on;
title('Ф-ия неопределенности для смещения по доплеру');
xlabel('Задержка');
ylabel('Частота Допплера');
zlabel('Нормированная неоднозначность');
